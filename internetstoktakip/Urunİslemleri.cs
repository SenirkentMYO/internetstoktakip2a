﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace internetstoktakip
{
    public class Urunİslemleri
    {
        public Vstok UrunListesi(int StokID, string UrunAdi)
        {
            List<Vstok> stk = new List<Vstok>();
            SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["baglantıCumlesi"].ToString());
            baglanti.Open();
            SqlCommand komut = new SqlCommand();
            komut.Connection = baglanti;
            komut.CommandType = CommandType.StoredProcedure;
            komut.CommandText = "UrunSP";

            SqlParameter p1 = new SqlParameter();
            p1.DbType = DbType.Int32;
            p1.ParameterName = "@StokID";
            p1.Value = StokID;

            SqlParameter p2 = new SqlParameter();
            p2.DbType = DbType.String;
            p2.ParameterName = "@UrunAdi";
            p2.Value = UrunAdi;

            komut.Parameters.Add(p1);
            komut.Parameters.Add(p2);

            SqlDataReader satir = komut.ExecuteReader();

            while (satir.Read())
            {
                Vstok st = new Vstok();
                st.StokID = (int)satir["StokID"];
                st.UrunAdi = satir["UrunAdi"].ToString();
                st.UrunMarkasi = satir["UrunMarkasi"].ToString();
                st.GelisFiyati = satir["GelisFiyati"].ToString();
                st.SatisFiyati = satir["SatisFiyati"].ToString();
                st.ToplamAdeti = (int)satir["ToplamAdeti"];
                st.StokAdeti = (int)satir["StokAdeti"];
                st.HaftalikSatisAdeti = (int)satir["HaftalikSatisAdeti"];
                stk.Add(st);
            }
            baglanti.Close();
            baglanti.Dispose();

            if (stk.Count > 0)
            {
                return stk[0];
            }
            else
            {
                return null;
            }

        }
    }
}
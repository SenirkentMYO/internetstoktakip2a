﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace internetstoktakip
{
    public partial class StokGiriş : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["GirilenÜrün"] == null)
            {
                Response.Redirect("default.aspx");
            }
            else
            {
                Lbl.Text = "Stoklarımızda Mevcuttur" + ((Vstok)Session["GirilenÜrün"]).StokID + " " + ((Vstok)Session["GirilenÜrün"]).UrunAdi;
            }

            Vstokislemleri stok = new Vstokislemleri();
            List<Vstok> p = stok.StokListesi();


            DataList1.DataSource = p;
            DataList1.DataBind();

        }

    }
}

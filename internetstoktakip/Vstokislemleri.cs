﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace internetstoktakip
{
    public class Vstokislemleri
    {
        public List<Vstok> StokListesi()
        {
            List<Vstok> p = new List<Vstok>();
            using (SqlConnection Vstok = new SqlConnection(ConfigurationManager.ConnectionStrings["baglantıCumlesi"].ToString()))
            {
                Vstok.Open();

                string sorguCumlem = "select * from Vstok";
                SqlCommand komut = new SqlCommand(sorguCumlem, Vstok);
                SqlDataReader satir = komut.ExecuteReader();

                while (satir.Read())
                {
                    Vstok st = new Vstok();
                    st.StokID = (int)satir["StokID"];
                    st.UrunAdi = satir["UrunAdi"].ToString();
                    st.UrunMarkasi = satir["UrunMarkasi"].ToString();
                    st.GelisFiyati = satir["GelisFiyati"].ToString();
                    st.SatisFiyati = satir["SatisFiyati"].ToString();
                    st.ToplamAdeti = (int)satir["ToplamAdeti"];
                    st.StokAdeti = (int)satir["StokAdeti"];
                    st.HaftalikSatisAdeti = (int)satir["HaftalikSatisAdeti"];
                    p.Add(st);
                }
            }
            return p;
        }
    }
}
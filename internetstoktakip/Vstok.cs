﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace internetstoktakip
{
    public class Vstok
    {
        public int StokID
        {
            get;
            set;
        }
        public string UrunAdi
        {
            get;
            set;
        }
        public string UrunMarkasi
        {
            get;
            set;
        }
        public string GelisFiyati
        {
            get;
            set;
        }
        public string SatisFiyati
        {
            get;
            set;
        }
        public int ToplamAdeti
        {
            get;
            set;
        }
        public int StokAdeti
        {
            get;
            set;
        }
        public int HaftalikSatisAdeti
        {
            get;
            set;
        }
    }
}
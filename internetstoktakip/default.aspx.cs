﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace internetstoktakip
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Application["sayac"] == null)
            {
                Application["sayac"] = 1;
            }
            else
            {
                int sayi = (int)Application["sayac"];
                sayi++;
                Application["sayac"] = sayi;

                Lblsayac.Text = Application["sayac"].ToString();
            }

            List<Vstok> p = new List<Vstok>();
            using (SqlConnection Vstok = new SqlConnection(ConfigurationManager.ConnectionStrings["baglantıCumlesi"].ToString()))
            {
                Vstok.Open();

                string sorguCumlem = "select * from Vstok";
                SqlCommand komut = new SqlCommand(sorguCumlem, Vstok);
                SqlDataReader satir = komut.ExecuteReader();

                while (satir.Read())
                {
                    Vstok st = new Vstok();
                    st.StokID = (int)satir["StokID"];
                    st.UrunAdi = satir["UrunAdi"].ToString();
                    st.UrunMarkasi = satir["UrunMarkasi"].ToString();
                    st.GelisFiyati = satir["GelisFiyati"].ToString();
                    st.SatisFiyati = satir["SatisFiyati"].ToString();
                    st.ToplamAdeti = (int)satir["ToplamAdeti"];
                    st.StokAdeti = (int)satir["StokAdeti"];
                    st.HaftalikSatisAdeti=(int)satir["HaftalikSatisAdeti"];
                    p.Add(st);
                }
                Vstok.Close();
                Vstok.Dispose();

                Repeater1.DataSource = p;
                Repeater1.DataBind();
            }

        }

        protected void Button1_Click1(object sender, EventArgs e)
        {

            List<Vstok> s = new List<Vstok>();
            using (SqlConnection stoktakip = new SqlConnection(ConfigurationManager.ConnectionStrings["baglantıCumlesi"].ToString()))
            {
                stoktakip.Open();

                string sorguCumlem = "select * from Vstok where  StokID= " + TextBox1.Text + " or UrunAdi ='" + TextBox2.Text + "'";
                SqlCommand komut = new SqlCommand(sorguCumlem, stoktakip);
                SqlDataReader satir = komut.ExecuteReader();


                while (satir.Read())
                {
                    Vstok st = new Vstok();
                    st.StokID=(int)satir["StokID"];
                    st.UrunAdi=satir["UrunAdi"].ToString();
                    st.UrunMarkasi = satir["UrunMarkasi"].ToString();
                    st.GelisFiyati = satir["GelisFiyati"].ToString();
                    st.SatisFiyati = satir["SatisFiyati"].ToString();
                    st.ToplamAdeti = (int)satir["ToplamAdeti"];
                    st.StokAdeti = (int)satir["StokAdeti"];
                    st.HaftalikSatisAdeti=(int)satir["HaftalikSatisAdeti"];
                    s.Add(st);
                }
                stoktakip.Close();
                stoktakip.Dispose();

         
                
                if (s.Count > 0)
                {
                    Session["GirilenÜrün"] = s[0];
                    Response.Redirect("StokGiriş.aspx");
                   // Label1.Text="HOŞGELDİNİZ"+s[0].UrunAdi+" "+s[0].UrunMarkasi+" ";
                }
                else
                {
                    Session["GirilenÜrün"] = null;
                    Label1.Text= "HATALI GİRİŞ";
                }
            }
        }
   }
}